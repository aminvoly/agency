<?php

namespace App\Tests;

use App\Entity\Hotel;
use App\Entity\Location;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BaseControllerTest extends WebTestCase
{
    const CATEGORIES = ['hotel', 'alternative', 'hostel', 'lodge', 'resort', 'guest-house'];
    /**
     * @var EntityManagerInterface
     */
    protected $manager;
    private $encoder;
    protected $faker;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->manager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->encoder = $kernel->getContainer()->get('security.password_encoder');
        $this->faker = Factory::create();
    }

    public function testSample()
    {
        $this->assertTrue(true);
    }

    protected function createTestUser($roles = ['ROLE_HOTELIER'])
    {
        $user = new User();
        $user->setEmail($this->faker->unique()->email)
            ->setPassword($this->encoder->encodePassword($user, '123456'))
            ->setRoles($roles);

        $this->manager->persist($user);

        $this->manager->flush();

        return $user;
    }

    protected function createTestHotel($user)
    {
        $hotel = new Hotel();
        $hotel->setName($this->faker->name)
            ->setRating($this->faker->numberBetween(0, 5))
            ->setCategory(self::CATEGORIES[array_rand(self::CATEGORIES)])
            ->setLocation($this->createTestLocation())
            ->setUser($user)
            ->setImage($this->faker->imageUrl())
            ->setReputation($this->faker->numberBetween(0, 1000))
            ->setPrice($this->faker->numberBetween(50, 1000))
            ->setAvailability($this->faker->numberBetween(1, 100));

        $this->manager->persist($hotel);

        $this->manager->flush();

        return $hotel;
    }

    protected function createTestLocation()
    {
        $location = new Location();
        $location->setCountry($this->faker->country)
            ->setCity($this->faker->city)
            ->setAddress($this->faker->address)
            ->setState($this->faker->state)
            ->setZipCode($this->faker->numberBetween(10000, 99999));
        $this->manager->persist($location);
        $this->manager->flush();

        return $location;

    }

    protected function getToken($username)
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/login_check',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(
                [
                    'username' => $username,
                    'password' => '123456',
                ]
            )
        );

        $data = json_decode($client->getResponse()->getContent(), true);
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $client;
    }

}
