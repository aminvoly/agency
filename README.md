# Details
```
I Have used symfony4.4 for this project.
Some of the important bundles I have used are:
lexic-jwt => for authentication
symfony-security => for security
symfony/serializer
nelmio/api-doc-bundle => for Swagger and documentation
```
# Requirements
```bash
   PHP 7.2.26
   Mysql Ver 15.1 Distrib 10.4.11-MariaDB
```
# Installation
Please Follow the steps to run the project.
```bash
    After cloning the project,
    Make a copy of .env.example and name it .env also make another copy and name it .env.test.local for the tests
    config database parameters based on .env.example
    composer install
    ./bin/console doctrine:database:create
    ./bin/console doctrine:database:create --env=test
    ./bin/console make:migration
    ./bin/console doctrine:migrations:migrate
    ./bin/console doctrine:migrations:migrate --env=test
```

### Generate the SSH keys:
```
$ mkdir -p config/jwt
$ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
$ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
Replace the JWT_PASSPHRASE value in .env and .env.test with the value 
you have been asked in the above steps
```

# To have mock data
```bash
./bin/console doctrine:fixtures:load
``` 
# Built in server
```bash
If you would like,You can use symfony web server to run the project by running this command:
./bin/console/server:start
```

# To run the tests
```bash
./bin/phpunit
Tests will be run in test database and all the data will be deleted after test.
```

# Swagger
```
    I have used swagger, for testing and all the endpoints are available at /api/doc path.
```
# How to test the project
```
    - I have created a user table, the fixtures will create some users, with ROLE_USER and ROLE_HOTELIER.
    - For the crud operation on hotel entity, you must login with an hotelier, the password is 123456 for all users.
    - The login endpoint is also available in /api/doc page,pick a email from database and set password,submit, the result will be a token
    - Then on the Authorize part in /api/doc, save the Bearer token.(You must type the Bearer, and a space after it, then the token value).
    - For booking the hotel, you must login with a user, so you should login with an user and change the authorize value.
    - The test value for create and update hotel
    {
    "name": "Example name",
    "rating": 5,
    "category": "hotel",
    "location": {
        "city": "Cuernavaca",
        "state": "Morelos",
        "country": "Mexico",
        "zipCode": "62448",
        "address": "Boulevard Díaz Ordaz No. 9 Cantarranas"
    },
    "image": "image-url.com", 
    "reputation": 120, 
    "price": 1000, 
    "availability": 10 
}
```

# Bonus
```
    - For the filter part, I have create a filter service, which works for all the fields, relations and all the conditions.
    - Example: /api/hotels?filter[rating]=4&filter[location.city]=tehran&filter[availability][lt]=12&filter[availability][gt]=5&filter[category]=hotel&filter[reputation]=490
    - Note: I couldn't find how to send filters like this in the swagger, so please test this part with postman(or whatever tools you prefer), and also don't forget to add Authorization header to Bearer {token} in postman, and content-type to application/json
    - lt is lower than, and gt is greater than in the above example.
    - Hoteliers which are not the owner of a hotel, are not able to modify or access to its data.
```
