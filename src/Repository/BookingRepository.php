<?php

namespace App\Repository;

use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    // /**
    //  * @return Booking[] Returns an array of Booking objects
    //  */

    public function findBookingByUserAndHotelId(int $userId, int $hotelId)
    {
        return $this->createQueryBuilder('booking')
            ->andWhere('booking.user = :userId')
            ->andWhere('booking.hotel = :hotelId')
            ->setParameter('userId', $userId)
            ->setParameter('hotelId', $hotelId)
            ->getQuery()
            ->getResult();
    }


}
