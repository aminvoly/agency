<?php

namespace App\Entity;

use App\Validator\HotelName;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HotelRepository")
 * @ORM\Table(name="hotels")
 * @ORM\HasLifecycleCallbacks()
 */
class Hotel
{
    const CATEGORIES = ['hotel', 'alternative', 'hostel', 'lodge', 'resort', 'guest-house'];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"Default"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Default"})
     * @Assert\NotBlank()
     * @Assert\Length(min="10")
     * @HotelName()
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"Default"})
     * @Assert\NotBlank()
     * @Assert\Range(min="0",max="5")
     */
    private $rating;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Default"})
     * @Assert\NotBlank()
     * @Assert\Choice(choices=Hotel::CATEGORIES)
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"Default"})
     * @Assert\NotBlank()
     * @Assert\Url(protocols = {"http","https"},relativeProtocol=false)
     */
    private $image;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"Default"})
     * @Assert\NotBlank()
     * @Assert\Range(min="0",max="1000")
     */
    private $reputation;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"Default"})
     * @Assert\NotBlank()
     * @Assert\Positive()
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"Default"})
     * @Assert\PositiveOrZero()
     */
    private $availability;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Location", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"Default"})
     * @Assert\Valid()
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="hotels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking", mappedBy="hotel",orphanRemoval=true)
     */
    private $bookings;

    public function __construct()
    {
        $this->bookings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getReputation(): ?int
    {
        return $this->reputation;
    }

    /**
     * @Groups({"Default"})
     */
    public function getReputationBadge(): ?string
    {
        if ($this->getReputation() <= 500) {
            return 'Red';
        } elseif ($this->getReputation() > 500 && $this->getReputation() <= 799) {
            return 'Yellow';
        }

        return 'Green';
    }

    public function setReputation(?int $reputation): self
    {
        $this->reputation = $reputation;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAvailability(): ?int
    {
        return $this->availability;
    }

    public function setAvailability(?int $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new \DateTimeImmutable('now');

        $this->setUpdatedAt($dateTimeNow);

        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(?Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setHotel($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getHotel() === $this) {
                $booking->setHotel(null);
            }
        }

        return $this;
    }
}
