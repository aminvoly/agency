<?php

namespace App\Services\ORM\Extension;

use Doctrine\ORM\QueryBuilder;

/**
 * Class QueryParameterNameGenerator.
 */
final class QueryAliasNameGenerator
{
    /**
     * @var int
     */
    private static $parameterNumber = 1;

    /**
     * @var array
     */
    private static $aliases = [];

    /**
     * @param string $association
     *
     * @return string
     */
    public static function generate(string $association): ?string
    {
        if (isset(self::$aliases[$association])) {
            return null;
        }

        return self::$aliases[$association] = str_replace('.', '_', $association).'_'.self::$parameterNumber++;
    }

    /**
     * @param QueryBuilder $builder
     * @param string       $association
     *
     * @return string|void
     */
    public static function getJoinAlias(QueryBuilder $builder, string $association)
    {
        if (!isset(self::$aliases[$association])) {
            return;
        }

        return self::$aliases[$association];
    }
}
