<?php

namespace App\Services\ORM\Extension;

use Doctrine\ORM\QueryBuilder;

/**
 * Interface QueryBuilderExtensionInterface.
 */
interface QueryBuilderExtensionInterface
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $resourceClass
     * @param array        $context
     */
    public function applyToCollection(QueryBuilder $queryBuilder, string $resourceClass, array &$context);
}
