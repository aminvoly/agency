<?php

namespace App\Services\ORM;

use App\Services\ORM\Extension\QueryBuilderExtensionInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * Class QueryBuilderFilterService.
 */
final class QueryBuilderFilterService
{
    /**
     * @var QueryBuilderExtensionInterface[]
     */
    private $collectionExtensions;

    /**
     * QueryBuilderFilterService constructor.
     *
     * @param $collectionExtensions
     */
    public function __construct(iterable $collectionExtensions)
    {
        $this->collectionExtensions = $collectionExtensions;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $resourceClass
     * @param array        $context
     */
    public function filter(QueryBuilder $queryBuilder, string $resourceClass, array $context = [])
    {
        if (!$context) {
            return;
        }

        foreach ($this->collectionExtensions as $extension) {
            $extension->applyToCollection($queryBuilder, $resourceClass, $context);
        }
    }
}
