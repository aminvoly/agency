<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Location;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class OrderItemFixture.
 */
class UserFixtures extends BaseFixture
{


    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * AdminFixture constructor.
     *
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * {@inheritdoc}
     */
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(
            User::class,
            10,
            function (User $item, $i) {
                $item->setEmail($this->faker->unique()->email)
                    ->setPassword($this->encoder->encodePassword($item, 123456))
                    ->setRoles(['ROLE_HOTELIER']);
            }
        );

        $this->createMany(
            User::class,
            10,
            function (User $item, $i) {
                $item->setEmail($this->faker->unique()->email)
                    ->setPassword($this->encoder->encodePassword($item, 123456))
                    ->setRoles(['ROLE_USER']);
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 11;
    }

    /**
     * {@inheritdoc}
     */
    protected function getReferenceName(string $className, int $i): string
    {
        return parent::getReferenceName($className, $i).'_'.uniqid();
    }
}
