<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Location;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class OrderItemFixture.
 */
class HotelFixtures extends BaseFixture
{
    const CATEGORIES = ['hotel', 'alternative', 'hostel', 'lodge', 'resort', 'guest-house'];

    /**
     * {@inheritdoc}
     */
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(
            Hotel::class,
            10,
            function (Hotel $item, $i) {
                $item->setName($this->faker->name)
                    ->setRating($this->faker->numberBetween(0, 5))
                    ->setCategory(self::CATEGORIES[array_rand(self::CATEGORIES)])
                    ->setLocation($this->getReference('location#'.($i + 1)))
                    ->setUser($this->getRandomReference(User::class))
                    ->setImage($this->faker->imageUrl())
                    ->setReputation($this->faker->numberBetween(0, 1000))
                    ->setPrice($this->faker->numberBetween(50, 1000))
                    ->setAvailability($this->faker->numberBetween(1, 100));
//                    ->setCreatedAt($this->faker->dateTimeThisYear)
//                    ->setUpdatedAt($this->faker->dateTimeThisYear);
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 12;
    }

    /**
     * {@inheritdoc}
     */
    protected function getReferenceName(string $className, int $i): string
    {
        return parent::getReferenceName($className, $i).'_'.uniqid();
    }
}
