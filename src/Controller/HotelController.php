<?php

namespace App\Controller;

use App\Entity\Hotel;
use App\Form\HotelType;
use App\Services\ORM\QueryBuilderFilterService;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as AccessSecurity;
use Swagger\Annotations as SWG;

use Nelmio\ApiDocBundle\Annotation\Model;

class HotelController extends BaseController
{

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     *
     * @Security(name="Bearer")
     *
     * @SWG\Tag(name="Hotel")
     * @SWG\Response(
     *     response=200,
     *     description="Get a single hotel"
     * )
     * @AccessSecurity("user.getId() == hotel.getUser().getId()")
     * @Route("/api/hotels/{id}", name="single_hotel",methods={"GET"})
     * @param Hotel $hotel
     * @return JsonResponse
     */
    public function hotel(Hotel $hotel)
    {
        return $this->respond($hotel);
    }

    /**
     * @Route("/api/hotels", name="hotels_list",methods={"GET"})
     *
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *     name="filter[]",
     *     in="query",
     *     type="array",
     *     uniqueItems=true,
     *     collectionFormat="multi",
     *     @SWG\Items(type="string"),
     *     description="Allow filtering response based on resource fields and relationships. example:
     *     filter[id]=10&filter[rating]=4&filter[location.city]=tehran&filter[availability][lt]=12&filter[availability][gt]=5&filter[category]=hotel&filter[reputation]=490"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns the array of hotles for logged in hotelier",
     * )
     * @SWG\Tag(name="Hotel")
     * @Security(name="Bearer")
     * @param QueryBuilderFilterService $filterService
     * @return JsonResponse
     */
    public function index(Request $request, QueryBuilderFilterService $filterService)
    {
        $userId = ($this->getUser()->getId());
        /** @var \Doctrine\ORM\QueryBuilder $hotels */
        $hotels = $this->manager->getRepository(Hotel::class)->findAllHotels($userId);
        $filterService->filter($hotels, Hotel::class, $request->query->all());

        return $this->respond($hotels->getQuery()->getResult());
    }

    /**
     * @Security(name="Bearer")
     *
     * @SWG\Tag(name="Hotel")
     * @SWG\Parameter(
     *     name="Body Parameters",
     *     in="body",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="name", type="string",description="A hotel name cannot contain the words Free ,Offer, Book, Website and it should be longer than 10 characters"),
     *         @SWG\Property(property="rating", type="integer",description="between 0 and 5"),
     *         @SWG\Property(property="category", type="string",description="The category can be one of [hotel, alternative, hostel, lodge, resort, guest-house] and it should be a string
    "),
     *         @SWG\Property(
     *             property="location",
     *             type="object",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="city", type="string"),
     *                 @SWG\Property(property="state", type="string"),
     *                 @SWG\Property(property="country", type="string"),
     *                 @SWG\Property(property="zipCode", type="integer",description="The zip code MUST be an integer and must have a length of 5."),
     *                 @SWG\Property(property="address", type="string")
     *             )
     *         ),
     *         @SWG\Property(property="image", type="string",description="Should be a valid url"),
     *         @SWG\Property(property="reputation", type="integer",description="between 0 and 1000"),
     *         @SWG\Property(property="price", type="integer"),
     *         @SWG\Property(property="availability", type="integer"),
     *     )
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Return created hotel",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Hotel::class, groups={"Default"}))
     *     )
     * )
     *
     * @Route("/api/hotels", name="store_hotel",methods={"POST"})
     *
     *
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $hotel = new Hotel();
        $form = $this->createForm(HotelType::class, $hotel, ['method' => 'POST']);
        $form->submit($request->request->all());
        if ($form->isValid() && $form->isSubmitted()) {
            $hotel->setUser($this->getUser());
            $this->manager->persist($hotel);
            $this->manager->flush();

            return $this->respond($hotel, Response::HTTP_CREATED);
        }

        return $this->respondValidatorFailed($form);
    }


    /**
     * @Security(name="Bearer")
     *
     * @SWG\Tag(name="Hotel")
     * @SWG\Parameter(
     *     name="Body Parameters",
     *     in="body",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="name", type="string",description="A hotel name cannot contain the words Free ,Offer, Book, Website and it should be longer than 10 characters"),
     *         @SWG\Property(property="rating", type="integer",description="between 0 and 5"),
     *         @SWG\Property(property="category", type="string",description="The category can be one of [hotel, alternative, hostel, lodge, resort, guest-house] and it should be a string
    "),
     *         @SWG\Property(
     *             property="location",
     *             type="object",
     *             @SWG\Items(
     *                 type="object",
     *                 @SWG\Property(property="city", type="string"),
     *                 @SWG\Property(property="state", type="string"),
     *                 @SWG\Property(property="country", type="string"),
     *                 @SWG\Property(property="zipCode", type="integer",description="The zip code MUST be an integer and must have a length of 5."),
     *                 @SWG\Property(property="address", type="string")
     *             )
     *         ),
     *         @SWG\Property(property="image", type="string",description="Should be a valid url"),
     *         @SWG\Property(property="reputation", type="integer",description="between 0 and 1000"),
     *         @SWG\Property(property="price", type="integer"),
     *         @SWG\Property(property="availability", type="integer"),
     *     )
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return updated hotel",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Hotel::class, groups={"Default"}))
     *     )
     * )
     *
     * @AccessSecurity("user.getId() == hotel.getUser().getId()")
     * @Route("/api/hotels/{id}", name="update_hotel",methods={"PUT"})
     * @param Hotel $hotel
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Hotel $hotel, Request $request)
    {
        $form = $this->createForm(HotelType::class, $hotel, ['method' => 'PUT']);
        $form->submit($request->request->all());
        if ($form->isValid() && $form->isSubmitted()) {
            $this->manager->persist($hotel);
            $this->manager->flush();

            return $this->respond($hotel);
        }

        return $this->respondValidatorFailed($form);
    }

    /**
     *
     * @Security(name="Bearer")
     *
     * @SWG\Tag(name="Hotel")
     * @SWG\Response(
     *     response=200,
     *     description="Delete a Hotel"
     * )
     * @AccessSecurity("user.getId() == hotel.getUser().getId()")
     * @Route("/api/hotels/{id}", name="delete_hotel",methods={"DELETE"})
     * @param Hotel $hotel
     * @return JsonResponse
     */
    public function delete(Hotel $hotel)
    {
        $this->manager->remove($hotel);
        $this->manager->flush();

        return $this->respond(['success' => 'Record deleted successfully!']);
    }

}
