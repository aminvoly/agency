<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends AbstractController
{
    private $hasErrors = false;
    private $message = 'Response successfully returned';

    /**
     * Sends serialized json response with default serialization group to client.
     *
     * @param array $data
     * @param int $statusCode
     * @param array $headers
     * @param array $context
     *
     * @return JsonResponse
     */
    protected function respond(
        $data = [],
        int $statusCode = Response::HTTP_OK,
        array $headers = [],
        array $context = ['groups' => 'Default']
    ): JsonResponse {
        return $this->json($data, $statusCode, $headers, $context);
    }

    /**
     * Responds validation error with messages.
     * @param FormInterface $form
     * @return JsonResponse
     */
    protected function respondValidatorFailed(FormInterface $form): JsonResponse
    {
        $errors = [];
        foreach ($form->getErrors(true, true) as $error) {
            $errors[$error->getOrigin()->getName()][] = $error->getMessage();
        }

        return $this->respondWithError(
            'Validation error has been detected!',
            $errors,
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }

    /**
     * Sends a response with error.
     *
     * @param string message
     * @param array $constraints
     * @param int $status
     * @return JsonResponse
     */
    protected function respondWithError(
        string $message = 'Error has been detected!',
        array $constraints = [],
        int $status = Response::HTTP_INTERNAL_SERVER_ERROR
    ): JsonResponse {
        return $this->setHasErrors(true)
            ->setMessage($message)
            ->respond($constraints, $status);
    }

    protected function setHasErrors(bool $hasErrors): self
    {
        $this->hasErrors = $hasErrors;

        return $this;
    }

    /**
     * @param string $message
     * @return $this
     */
    protected function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

}
